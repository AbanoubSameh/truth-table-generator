package tableGenerator;

abstract class Divider {

	// method used to divide the letters in various andGates
	static String[] divide(String input) {

		String temp = "";
		// System.out.println(count(input));

		int counter = 0;
		String[] output = new String[count(input)]; // calls count in order to get string array length

		// cycles throw all chars until it finds a non alphabetic
		for (int i = 0; i < input.length(); i++) {

			if (Character.isAlphabetic(input.charAt(i))) {

				temp += String.valueOf(input.charAt(i));

			} else {

				// stores found values **
				output[counter] = temp;
				counter++;
				temp = "";

			}

		}

		// System.out.println(count(input));
		output[counter] = temp;
		temp = "";

		return output;

	}

	// does pretty much the same, does not save results though, works just to
	// calculate the length of string before hand
	private static int count(String input) {

		int first = 0;
		String temp = "";
		int counter = 0;

		for (int i = 0; i < input.length(); i++) {

			if (Character.isAlphabetic(input.charAt(i))) {

				temp = "" + input.substring(first, i + 1);

			} else if (!Character.isAlphabetic(input.charAt(i))) {

				if (temp.length() > 0) {

					counter++;
					temp = "";

				}

			}

		}

		if (temp.length() > 0) {

			counter++;
			temp = "";

		}

		return counter;

	}

}
