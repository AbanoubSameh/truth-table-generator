package tableGenerator;

// all of these are logic gates that accept an int array of any combination of ones or zeros
abstract class VLogicGates {

	// and gate, returns true only if all values are ones
	public static class vandGate {

		// variables
		private int[] x;
		private String y;

		// constructor
		public vandGate(int[] x) {

			// determines if array contains invalid numbers, if so makes an error on purpose
			for (int i = 0; i < x.length; i++) {
				if (x[i] != 0 && x[i] != 1) {
					x[i] = Integer.parseInt(null);
				}
			}

			this.x = x;
		}

		// sets inputs
		public void setInput(int input, int value) {
			if (value != 0 && value != 1) {
				x[input] = Integer.parseInt(null);
			} else {
				x[input] = value;
			}
		}

		// gets y
		public int getY() {

			String ret = "1";

			for (int i = 0; i < x.length; i++) {
				if (x[i] == 0) {
					ret = "0";
				} else if (x[i] != 0 && x[i] != 1) {
					ret = null;
					break;
				}
			}

			// calculate y
			if (ret == "0") {
				this.y = "0";
			} else if (ret == "1") {
				this.y = "1";
			}

			return Integer.parseInt(y);
		}

	}

	// all are the same except for or it returns 0 if all inputs are 0
	public static class vorGate {

		private int[] x;
		private String y;

		public vorGate(int[] x) {

			for (int i = 0; i < x.length; i++) {
				if (x[i] != 0 && x[i] != 1) {
					x[i] = Integer.parseInt(null);
				}
			}

			this.x = x;
		}

		public void setInput(int input, int value) {
			if (value != 0 && value != 1) {
				x[input] = Integer.parseInt(null);
			} else {
				x[input] = value;
			}
		}

		public int getY() {

			String ret = "0";

			for (int i = 0; i < x.length; i++) {
				if (x[i] == 1) {
					ret = "1";
				} else if (x[i] != 0 && x[i] != 1) {
					ret = null;
					break;
				}
			}

			if (ret == "0") {
				this.y = "0";
			} else if (ret == "1") {
				this.y = "1";
			}

			return Integer.parseInt(y);
		}

	}

	// here it returns 1 if number of one in array is odd **
	public static class vxorGate {

		private int[] x;
		private String y;

		public vxorGate(int[] x) {

			for (int i = 0; i < x.length; i++) {
				if (x[i] != 0 && x[i] != 1) {
					x[i] = Integer.parseInt(null);
				}
			}

			this.x = x;
		}

		public void setInput(int input, int value) {
			if (value != 0 && value != 1) {
				x[input] = Integer.parseInt(null);
			} else {
				x[input] = value;
			}
		}

		public int getY() {

			int ret = 0;

			for (int i = 0; i < x.length; i++) {
				if (x[i] == 1) {
					ret++;
				} else if (x[i] != 0 && x[i] != 1) {
					ret = -1;
					break;
				}
			}

			if (ret == -1) {
				this.y = null;
			} else if (ret % 2 == 0 || ret == 0) {
				this.y = "0";
			} else if (ret % 2 != 0) {
				this.y = "1";
			}

			return Integer.parseInt(y);
		}

	}

	// for here on all gate return the opposite of the others above
	public static class vnandGate {

		private int[] x;
		private String y;

		public vnandGate(int[] x) {

			for (int i = 0; i < x.length; i++) {
				if (x[i] != 0 && x[i] != 1) {
					x[i] = Integer.parseInt(null);
				}
			}

			this.x = x;
		}

		public void setInput(int input, int value) {
			if (value != 0 && value != 1) {
				x[input] = Integer.parseInt(null);
			} else {
				x[input] = value;
			}
		}

		public int getY() {

			String ret = "1";

			for (int i = 0; i < x.length; i++) {
				if (x[i] == 0) {
					ret = "0";
				} else if (x[i] != 0 && x[i] != 1) {
					ret = null;
					break;
				}
			}

			if (ret == "0") {
				this.y = "1";
			} else if (ret == "1") {
				this.y = "0";
			}

			return Integer.parseInt(y);
		}

	}

	public static class vnorGate {

		private int[] x;
		private String y;

		public vnorGate(int[] x) {

			for (int i = 0; i < x.length; i++) {
				if (x[i] != 0 && x[i] != 1) {
					x[i] = Integer.parseInt(null);
				}
			}

			this.x = x;
		}

		public void setInput(int input, int value) {
			if (value != 0 && value != 1) {
				x[input] = Integer.parseInt(null);
			} else {
				x[input] = value;
			}
		}

		public int getY() {

			String ret = "0";

			for (int i = 0; i < x.length; i++) {
				if (x[i] == 1) {
					ret = "1";
				} else if (x[i] != 0 && x[i] != 1) {
					ret = null;
					break;
				}
			}

			if (ret == "0") {
				this.y = "1";
			} else if (ret == "1") {
				this.y = "0";
			}

			return Integer.parseInt(y);
		}

	}

	public static class vxnorGate {

		private int[] x;
		private String y;

		public vxnorGate(int[] x) {

			for (int i = 0; i < x.length; i++) {
				if (x[i] != 0 && x[i] != 1) {
					x[i] = Integer.parseInt(null);
				}
			}

			this.x = x;
		}

		public void setInput(int input, int value) {
			if (value != 0 && value != 1) {
				x[input] = Integer.parseInt(null);
			} else {
				x[input] = value;
			}
		}

		public int getY() {

			int ret = 0;

			for (int i = 0; i < x.length; i++) {
				if (x[i] == 1) {
					ret++;
				} else if (x[i] != 0 && x[i] != 1) {
					ret = -1;
					break;
				}
			}

			if (ret == -1) {
				this.y = null;
			} else if (ret % 2 == 0 || ret == 0) {
				this.y = "1";
			} else if (ret % 2 != 0) {
				this.y = "0";
			}

			return Integer.parseInt(y);
		}

	}

}
