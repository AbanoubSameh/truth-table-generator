package tableGenerator;

import tableGenerator.VLogicGates.*;

// This is the only class that is meant to be visible outside so that you can call it, give it a string and see it roll
public abstract class TableGenerator {

	public static void generateTable(String input) { // The only public method

		String[] andGates = Divider.divide(input); // Calls method to divide the string in and gates

		String base = LengthCalculator.getLength(input); // Calls method to remove duplicates then get length of string

		/*
		 * for (int i = 0; i < andGates.length; i++) {
		 * 
		 * System.out.println("andGates " + i + ": " + andGates[i]);
		 * 
		 * }
		 */

		vandGate[] test = new vandGate[andGates.length]; // Creates necessary andGates
		int[] randomName; // int arrays to store values for various andGates

		vorGate end; // Final orGate used to sum everything together at the end

		int[][] answer = new int[(int) Math.pow(2, base.length())][andGates.length]; // Matrix used to store answers for
																						// each gate

		int[][] table = TablePrinter.table(base.length()); // Table to be generated

		int[] endish = new int[(int) Math.pow(2, base.length())]; // Variable to store final result

		// Complicated for loops that does all the work

		// Runs for the ** y axis **
		for (int i = 0; i < (int) Math.pow(2, base.length()); i++) {

			// Runs for each andGate
			for (int j = 0; j < andGates.length; j++) {

				randomName = new int[andGates[j].length()];
				// System.out.println(fuck.length);

				// Runs for each variable inside THE andGate
				for (int k = 0; k < andGates[j].length(); k++) {

					// System.out.println(base.indexOf(andGates[j].charAt(k)));
					randomName[k] = table[base.indexOf(andGates[j].charAt(k))][i];
					// System.out.println(fuck[k]);

				}

				test[j] = new vandGate(randomName);
				answer[i][j] = test[j].getY();

			}

			end = new vorGate(answer[i]);

			// System.out.println(end.getY());
			endish[i] = end.getY();

		}

		TablePrinter.printFullTable(base, table, endish); // Prints table

	}

}
