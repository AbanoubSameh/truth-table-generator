package tableGenerator;

abstract class TablePrinter {

	// basically all of these functions use for loops to generate tables and
	// matrixes

	// this one generate the ones and zeros used in the truth table
	static int[][] table(int nov) {

		int x = nov; // number of variables (length of x)

		int y = (int) Math.pow(2, nov); // (length of y)

		int[][] table = new int[x][y]; // table (matrix)

		int len = 1; // length starts with one

		// runs for every row **
		for (int i = x - 1; i >= 0; i--) {

			// runs for every column **
			for (int j = 0; j < y; j += len * 2) {

				// generates zeros
				for (int f = 0; f < len; f++) {

					// System.out.println(j + f + ": 0");
					table[i][j + f] = 0;

				}

				// generates ones
				for (int f = 0; f < len; f++) {

					// System.out.println(j + f + len + ": 1");
					table[i][j + f + len] = 1;

				}

			}

			len *= 2;

		}

		return table; // returns finished table

	}

	// this one prints the table
	static void printTable(int[][] table) {

		int x = table.length; // length of x
		int y = (int) Math.pow(2, x); // length of y

		// runs for every column **
		for (int i = 0; i < y; i++) {

			System.out.print("| ");

			// runs for every row **
			for (int j = 0; j < x; j++) {

				System.out.print(table[j][i]);

				// not to print more symbols at the last cycle
				if (j != x - 1) {

					System.out.print(" | ");

				}

			}

			System.out.println(" |");

		}

	}

	// this one also does the same thing but better and it includes the answer
	// column
	static void printFullTable(String input, int[][] table, int[] answer) {

		int x = table.length; // length of x
		int y = (int) Math.pow(2, x); // length of y

		// !!VERY COMPLICATED STUFF, NOT TO BE MESSED WITH!!

		System.out.print("+");

		// first row of ----
		for (int i = 0; i < 5 * x - (x - 1) + 3; i++) {

			System.out.print("-");

		}

		System.out.println("+");
		System.out.print("| ");

		// table begins here:
		for (int i = 0; i < input.length(); i++) {

			System.out.print(input.charAt(i));

			// not to print more symbols on the last cycle
			if (i != input.length() - 1) {

				System.out.print(" | ");

			}

		}

		// just to make everything tidy
		System.out.println(" || y |");

		System.out.print("+");

		// more ----
		for (int i = 0; i < 5 * x - (x - 1) + 3; i++) {

			System.out.print("-");

		}

		System.out.println("+");

		// more stuff coming your way
		for (int i = 0; i < y; i++) {

			System.out.print("| ");

			for (int j = 0; j < x; j++) {

				System.out.print(table[j][i]);

				if (j != x - 1) {

					System.out.print(" | ");

				}

			}

			System.out.println(" || " + answer[i] + " |");
			
			if((i - 1) % 2 == 0) {
				
				System.out.print("+");

				// first row of ----
				for (int h = 0; h < 5 * x - (x - 1) + 3; h++) {

					System.out.print("-");

				}

				System.out.println("+");
				
			}

		}

	}

}
