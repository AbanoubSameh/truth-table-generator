package tableGenerator;

abstract class LengthCalculator {

	// used to generate pure string without duplicates
	static String getLength(String input) {

		input = input.replaceAll("[^a-zA-Z]", ""); // complicated java lang which means to replace all non alphabetics

		String chars = ""; // define the string that will hold the answer

		for (int i = 0; i < input.length(); i++) {

			if (!chars.contains(String.valueOf(input.charAt(i)))) {
				chars += input.charAt(i);
			}

		}

		return chars;

	}

}
